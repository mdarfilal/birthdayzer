package com.mdarfilal.birthdayzer;

import com.mdarfilal.birthdayzer.adapters.secondaries.CsvEmployeesLoader;
import com.mdarfilal.birthdayzer.adapters.secondaries.LoggerBirthdayNotifier;
import com.mdarfilal.birthdayzer.usecase.BirthdayNotifierUseCase;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.time.LocalDate;

@SpringBootApplication
public class BirthdayzerApplication {

    public static void main(String[] args) {
        SpringApplication.run(BirthdayzerApplication.class, args);

        BirthdayNotifierUseCase uc = new BirthdayNotifierUseCase(LocalDate.now(), new LoggerBirthdayNotifier(), new CsvEmployeesLoader());

        uc.handle();
    }

}
