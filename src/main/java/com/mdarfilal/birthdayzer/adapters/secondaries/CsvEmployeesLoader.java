package com.mdarfilal.birthdayzer.adapters.secondaries;

import com.mdarfilal.birthdayzer.domain.Anniversary;
import com.mdarfilal.birthdayzer.domain.Employee;
import com.mdarfilal.birthdayzer.domain.EmployeesLoader;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class CsvEmployeesLoader implements EmployeesLoader {

    private static final int HEADER = 1;
    private static final String CSV_EMPLOYEES_SEPARATOR = ",";
    private static final String CSV_EMPLOYEES_BIRTHDAYS_FORMAT = "yyyy/MM/dd";
    private static final String CSV_EMPLOYEES_FILE = "employees.csv";

    @Override
    public Set<Employee> all() {
        return loadEmployeeFromCsv()
                .stream()
                .skip(HEADER)
                .map(this::splitCsvEmployeeLine)
                .map(this::mapToEmployee)
                .collect(Collectors.toSet());
    }

    private List<String> loadEmployeeFromCsv() {
        try {
            return Files.readAllLines(Paths.get(getClass().getClassLoader().getResource(CSV_EMPLOYEES_FILE).toURI()));
        } catch (IOException | URISyntaxException e) {
            System.out.println("Cannot read file" + e.getMessage());
        }
        return new ArrayList<>();
    }

    private String[] splitCsvEmployeeLine(String e) {
        return e.split(CSV_EMPLOYEES_SEPARATOR);
    }

    private Employee mapToEmployee(String[] splitCsvEmployee) {
        LocalDate birthday = LocalDate.parse(splitCsvEmployee[2], DateTimeFormatter.ofPattern(CSV_EMPLOYEES_BIRTHDAYS_FORMAT));
        return new Employee(splitCsvEmployee[1],
                new Anniversary(birthday.getMonth(), birthday.getDayOfMonth()),
                splitCsvEmployee[3]);
    }

}
