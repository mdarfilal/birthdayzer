package com.mdarfilal.birthdayzer.adapters.secondaries;

import com.mdarfilal.birthdayzer.domain.BirthdayNotifier;
import com.mdarfilal.birthdayzer.domain.Employee;

public class LoggerBirthdayNotifier implements BirthdayNotifier {
    @Override
    public void notify(Employee employee) {
        System.out.println(employee.getFirstName() + " t'as veillit sal fou !!!");
    }
}
