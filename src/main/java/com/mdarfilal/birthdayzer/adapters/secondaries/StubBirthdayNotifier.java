package com.mdarfilal.birthdayzer.adapters.secondaries;

import com.mdarfilal.birthdayzer.domain.BirthdayNotifier;
import com.mdarfilal.birthdayzer.domain.Employee;

import java.util.HashSet;
import java.util.Set;

public class StubBirthdayNotifier implements BirthdayNotifier {

    private Set<Employee> birthdates = new HashSet<>();

    @Override
    public void notify(Employee employee) {
        birthdates.add(employee);
    }

    public Set<Employee> all() {
        return birthdates;
    }
}
