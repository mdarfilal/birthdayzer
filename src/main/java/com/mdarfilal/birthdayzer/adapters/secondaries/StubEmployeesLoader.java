package com.mdarfilal.birthdayzer.adapters.secondaries;

import com.mdarfilal.birthdayzer.domain.Employee;
import com.mdarfilal.birthdayzer.domain.EmployeesLoader;

import java.util.HashSet;
import java.util.Set;

public class StubEmployeesLoader implements EmployeesLoader {

    private Set<Employee> employees = new HashSet<>();

    public void add(Employee employee) {
        employees.add(employee);
    }

    @Override
    public Set<Employee> all() {
        return employees;
    }
}
