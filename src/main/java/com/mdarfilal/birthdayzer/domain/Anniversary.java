package com.mdarfilal.birthdayzer.domain;

import java.time.Month;

public class Anniversary {
    private int day;
    private Month month;

    public Anniversary(Month month, int day) {
        this.month = month;
        this.day = day;
    }

    int getDay() {
        return day;
    }

    Month getMonth() {
        return month;
    }
}
