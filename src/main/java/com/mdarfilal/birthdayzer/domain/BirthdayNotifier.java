package com.mdarfilal.birthdayzer.domain;

public interface BirthdayNotifier {
    void notify(Employee employee);
}
