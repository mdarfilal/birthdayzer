package com.mdarfilal.birthdayzer.domain;

import java.time.LocalDate;
import java.time.Month;

public class Employee {
    private static final int LAST_FEBRUARY_DAY_IN_LEAP_YEAR = 29;
    private static final int LAST_FEBRUARY_DAY_IN_COMMON_YEAR = 28;

    private final String firstName;
    private final Anniversary anniversary;
    private final String email;

    public Employee(String firstName, Anniversary anniversary, String email) {
        this.firstName = firstName;
        this.anniversary = anniversary;
        this.email = email;
    }

    public boolean isItHisAnniversary(LocalDate today) {
        return doDayAndMonthMatch(today) || isAnniversaryOfEmployeeBornOn29February(today);
    }

    private boolean doDayAndMonthMatch(LocalDate today) {
        return this.anniversary.getDay() == today.getDayOfMonth()
                && this.anniversary.getMonth().equals(today.getMonth());
    }

    private boolean isAnniversaryOfEmployeeBornOn29February(LocalDate today) {
        return anniversary.getDay() == LAST_FEBRUARY_DAY_IN_LEAP_YEAR
                && anniversary.getMonth().equals(Month.FEBRUARY)
                && !today.isLeapYear()
                && today.getDayOfMonth() == LAST_FEBRUARY_DAY_IN_COMMON_YEAR
                && today.getMonth().equals(Month.FEBRUARY);
    }

    public String getFirstName() {
        return firstName;
    }

}
