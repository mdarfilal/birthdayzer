package com.mdarfilal.birthdayzer.domain;

import java.util.Set;

public interface EmployeesLoader {
    Set<Employee> all();
}
