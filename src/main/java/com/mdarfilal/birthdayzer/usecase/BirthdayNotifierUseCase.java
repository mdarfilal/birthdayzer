package com.mdarfilal.birthdayzer.usecase;

import com.mdarfilal.birthdayzer.domain.BirthdayNotifier;
import com.mdarfilal.birthdayzer.domain.EmployeesLoader;

import java.time.LocalDate;

public class BirthdayNotifierUseCase {
    private LocalDate today;
    private BirthdayNotifier birthdayNotifier;
    private EmployeesLoader employeesLoader;

    public BirthdayNotifierUseCase(LocalDate today, BirthdayNotifier birthdayNotifier, EmployeesLoader employeesLoader) {
        this.today = today;
        this.birthdayNotifier = birthdayNotifier;
        this.employeesLoader = employeesLoader;
    }

    public void handle() {
        this.employeesLoader
                .all()
                .forEach(e -> {
                    if (e.isItHisAnniversary(today)) {
                        birthdayNotifier.notify(e);
                    }
                });
    }
}
