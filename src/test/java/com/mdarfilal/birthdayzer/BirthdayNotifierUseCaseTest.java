package com.mdarfilal.birthdayzer;

import com.mdarfilal.birthdayzer.adapters.secondaries.StubBirthdayNotifier;
import com.mdarfilal.birthdayzer.adapters.secondaries.StubEmployeesLoader;
import com.mdarfilal.birthdayzer.domain.Anniversary;
import com.mdarfilal.birthdayzer.domain.Employee;
import com.mdarfilal.birthdayzer.usecase.BirthdayNotifierUseCase;
import org.junit.Before;
import org.junit.Test;

import java.time.Month;

import static java.time.LocalDate.now;
import static java.time.LocalDate.of;
import static org.junit.Assert.assertEquals;

public class BirthdayNotifierUseCaseTest {

    private StubBirthdayNotifier birthdayNotifier;
    private StubEmployeesLoader employeesLoader;

    @Before
    public void setUp() {
        birthdayNotifier = new StubBirthdayNotifier();
        employeesLoader = new StubEmployeesLoader();
    }

    @Test
    public void shouldNotifyWhenItIsHisBirthday() {
        Employee john = new Employee("John", new Anniversary(now().getMonth(), now().getDayOfMonth()), "john.doe@foobar.com");
        employeesLoader.add(john);

        BirthdayNotifierUseCase uc = new BirthdayNotifierUseCase(now(), birthdayNotifier, employeesLoader);
        uc.handle();

        assertEquals(1, birthdayNotifier.all().size());
    }

    @Test
    public void shouldNotNotifyWhenItIsNotHisBirthday() {
        Employee john = new Employee("John", new Anniversary(Month.JANUARY, 1), "john.doe@foobar.com");
        employeesLoader.add(john);

        BirthdayNotifierUseCase uc = new BirthdayNotifierUseCase(of(2019, 2, 2), birthdayNotifier, employeesLoader);
        uc.handle();

        assertEquals(0, birthdayNotifier.all().size());
    }

    @Test
    public void shouldNotifyAllEmployeesForTheirBirthday() {
        Employee john = new Employee("John", new Anniversary(now().getMonth(), now().getDayOfMonth()), "john.doe@foobar.com");
        Employee franck = new Employee("Franck", new Anniversary(now().getMonth(), now().getDayOfMonth()), "franck.franky@foobar.com");
        employeesLoader.add(john);
        employeesLoader.add(franck);

        BirthdayNotifierUseCase uc = new BirthdayNotifierUseCase(now(), birthdayNotifier, employeesLoader);
        uc.handle();

        assertEquals(2, birthdayNotifier.all().size());
    }

    @Test
    public void shouldNotifyOnThe28thWhenEmployeeIsBornOnThe29thAndLeapYear() {
        Employee john = new Employee("John", new Anniversary(Month.FEBRUARY, 29), "john.doe@foobar.com");
        employeesLoader.add(john);

        BirthdayNotifierUseCase uc = new BirthdayNotifierUseCase(of(2019, 2, 28), birthdayNotifier, employeesLoader);
        uc.handle();

        assertEquals(1, birthdayNotifier.all().size());
    }

}
